import os

from flask import Flask
from sassutils.wsgi import SassMiddleware

app = Flask(__name__)

app.config['base_dir'] = os.path.dirname(__file__)
app.config['data_file'] = os.path.join(app.config['base_dir'], 'lat_long_data.pkl')
app.wsgi_app = SassMiddleware(app.wsgi_app, {
    'spacejunk': ('static/scss', 'static/css', '/static/css')
})

from . import views