$(document).ready(function () {
    replot();
})
$(window).resize(function () {
    replot();
})

function replot() {
    $.ajax({
        url: "/api/data",
        type: "GET",
        contentType: 'application/json;charset=UTF-8',
        success: function (data) {
            geo_plot(data);
            histograms(data);
        }
    })
};

function geo_plot(data) {
    var trace = [
    {
        type: 'scattergeo',
        hoverinfo: 'none',
        lon: data['payload']['longitude'],
        lat: data['payload']['latitude'],
        mode: 'markers',
        marker: {
            size: 7,
            opacity: .6,
            symbol: 'square',
            color: '#005995',
            line: { width: 0 },
        },
        name: 'Payload',
    },
    {
        type: 'scattergeo',
        hoverinfo: 'none',
        lon: data['rocket body']['longitude'],
        lat: data['rocket body']['latitude'],
        mode: 'markers',
        marker: {
            size: 7,
            opacity: .6,
            symbol: 'square',
            color: '#fbf579',
            line: { width: 0 },
        },
        name: 'Rocket Body',
    },
    {
        type: 'scattergeo',
        hoverinfo: 'none',
        lon: data['debris']['longitude'],
        lat: data['debris']['latitude'],
        mode: 'markers',
        marker: {
            size: 7,
            opacity: .6,
            symbol: 'square',
            color: '#fa625f',
            line: { width: 0 },
        },
        name: 'Debris',
    },
    {
        type: 'scattergeo',
        hoverinfo: 'none',
        lon: data['tba']['longitude'],
        lat: data['tba']['latitude'],
        mode: 'markers',
        marker: {
            size: 7,
            opacity: .6,
            symbol: 'square',
            color: '#600473',
            line: { width: 0 },
        },
        name: 'TBA',
    }];

    var layout = {
        geo: {
            projection: {
                type: "orthographic",
                rotation: { lon: -93.0977, lat: 41.8780 },
                scale: 1,
            },
            coastlinecolor: "#FFFFFF",
            showsubunits: true,
            showcountries: true,
            subunitcolor: "rgb(217, 217, 217)",
            countrycolor: "rgb(140, 140, 140)",
            countrywidth: 0.5,
            subunitwidth: 0.5,
            showframe: false,
            bgcolor: "#201c1b",
        },
        showlegend: true,
        legend: {
            orientation: 'h',
            font: { color: 'rgb(170, 170, 170)' }
        },
        margin: {
            b: 5,
            l: 5,
            r: 5,
            t: 5,
            pad: 5,
        },
        plot_bgcolor: "#201c1b",
        paper_bgcolor: "#201c1b",
        width: $("#plot").width(),
        height: $("#plot").width() * 1.2,
    };
    Plotly.newPlot("plot", trace, layout, { showLink: false });
};

function histograms(data) {
    var trace = [{
        type: 'histogram',
        x: data['all']['longitude'],
        showlegend: false,
        xbins: {
            start: -180,
            end: 180
        },
        marker: {
            color: '#fbf579'
        }
    }];

    var layout = {
        width: $("#hist_lat").width(),
        height: $("#hist_lat").width() * .8,
        plot_bgcolor : "#201c1b",
        paper_bgcolor : "#201c1b",
        xaxis : {color : "#FFFFFF", title: "Longitude (°E)"},
        yaxis : {color : "#FFFFFF"},
        margin: {
            b: 50,
            l: 50,
            r: 50,
            t: 50,
        },
    };

    Plotly.newPlot("hist_lat", trace, layout, { showLink: false });

    trace = [{
        type: 'histogram',
        y: data['all']['latitude'],
        showlegend: false,
        ybins: {
            start: -90,
            end: 90
        },
        marker: {
            color: '#fbf579'
        }
    }];

    layout = {
        width: $("#hist_lon").width(),
        height: $("#hist_lon").width() * .8,
        plot_bgcolor : "#201c1b",
        paper_bgcolor : "#201c1b",
        xaxis : {color : "#FFFFFF"},
        yaxis : {color : "#FFFFFF", title: "Latitude (°N)"},
        margin: {
            b: 50,
            l: 50,
            r: 50,
            t: 50,
        },
    };

    Plotly.newPlot("hist_lon", trace, layout, { showLink: false });
};