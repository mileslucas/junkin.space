
navigator.geolocation.getCurrentPosition(function (position) {
    $.ajax({
        url: "/api/prob",
        type: "GET",
        contentType: 'application/json;charset=UTF-8',
        dataType: "json",
        data: {
            lat: position.coords.latitude,
            lon: position.coords.longitude,
            accuracy: position.coords.accuracy,
        },
        success: function (response) {
            $("#prob").text(`a 1:${response}`);
        }
    })
});

$(document).ready(function() {
    var comps = [
        'Compare that to the 1:27,269,633 chance of winning the powerball!',
        'Compare that to the 1:3,000 chance of getting struck by lightning!',
        'Compare that to the 1:649,740 chance of getting a royal flush!',
        'Compare that to the 1:9,223,372,036,854,775,808 chance of having a perfect March Madness bracket!',
        'Compare that to the 1:150 chance of having identical twins!'
    ]
    var choice = comps[Math.floor(Math.random() * comps.length)];
    $("#comp").text(choice);
})

