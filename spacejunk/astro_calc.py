import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from datetime import datetime
import time

import pandas as pd
import numpy as np

from spacejunk import app


INPUTCOLS = ['OBJECT_NAME', 'OBJECT_TYPE', 'EPOCH',
             'MEAN_MOTION', 'ECCENTRICITY', 'INCLINATION',
             'RA_OF_ASC_NODE', 'ARG_OF_PERICENTER',
             'MEAN_ANOMALY', 'SEMIMAJOR_AXIS', 'PERIOD',
             'APOGEE', 'PERIGEE']


COLS = ['name', 'type', 'epoch', 'mean_motion',
        'ecc', 'inc', 'raan', 'arg_perigee',
        'mean_anom_epoch', 'a', 'period', 'apogee',
        'perigee']


human_tags = ['dragon', 'htv', 'cygnus',
              'soyuz', 'zarya', 'tiangong',
              'shenzhou', 'progess']


def get_mean_anomaly(data):
    difference = datetime.now() - data['epoch']
    anomaly = data['mean_motion'] * difference.total_seconds() + data['mean_anom_epoch']
    return anomaly % 360


def get_category(data):
    # Human Spaceflight
    for tag in human_tags:
        if tag in data['name'].lower():
            return 'Human'

    # Only want payloads
    if data['type'] != 'PAYLOAD':
        return None

    # Geostationary
    if (1430 < data['period'] < 1445 and
            -5 < data['inc'] < 5 and
            data['ecc'] < 0.1):
        return 'Geostationary'

    # Navigation Sats
    if 'NAVSTAR' in data['name']:
        return 'GPS'
    if 'GLONASS' in data['name']:
        return 'GLONASS'
    if 'GALILEO' in data['name']:
        return 'GALILEO'
    if 'IRNSS' in data['name']:
        return 'IRNSS'
    if 'BEIDOU' in data['name']:
        return 'Beidou'

    # LEO
    if (data['period'] < 128 and
            data['ecc'] < 0.25):
        return 'LEO'


def convert_coords(a, e, i, raan, arg_peri, mean_anom):
    """
    Andrew please write a summary here
    
    Parameters
    ----------
    a : [type]
        [description]
    e : [type]
        [description]
    i : [type]
        [description]
    raan : [type]
        [description]
    arg_peri : [type]
        [description]
    mean_anom : [type]
        [description]
    
    Returns
    -------
    [type]
        [description]
    """

    θ_g0 = 101.25
    t_0 = 946728000
    ω_earth = 4.18e-3  # deg/s
    θ_g = θ_g0 + ω_earth * (time.time() - t_0)

    i = np.deg2rad(i)
    raan = np.deg2rad(raan)
    arg_peri = np.deg2rad(arg_peri)
    mean_anom = np.deg2rad(mean_anom)

    ω_trans = np.array(
        [[np.cos(arg_peri), -np.sin(arg_peri), 0],
         [np.sin(arg_peri), np.cos(arg_peri), 0],
         [0, 0, 1]]
    )
    Ω_trans = np.array(
        [[np.cos(raan), -np.sin(raan), 0],
         [np.sin(raan), np.cos(raan), 0],
         [0, 0, 1]]
    )
    i_trans = np.array(
        [[1, 0, 0],
         [0, np.cos(i), np.sin(i)],
         [0, np.sin(i), np.cos(i)]]
    )
    Θ_trans = np.array(
        [[np.cos(θ_g), -np.sin(θ_g), 0],
         [np.sin(θ_g), np.cos(θ_g), 0],
         [0, 0, 1]]
    )

    true_anom = mean_anom + \
                 (2 * e + e ** 3 * 1 / 4) * np.sin(mean_anom) + \
                 (5 / 4) * e ** 2 * np.sin(2 * mean_anom) + \
                 (13 / 12) * e ** 3 * np.sin(3 * mean_anom)

    r = a * (1 - e ** 2) / (1 + e * np.cos(true_anom))
    x = r * np.cos(true_anom)
    y = r * np.sin(true_anom)
    xy = np.array([[x], [y], [0]])

    cartesian_coords = np.squeeze(Θ_trans @ Ω_trans @ i_trans @ ω_trans @ xy)
    X, Y, Z = cartesian_coords

    # Standard conversion from cartesian to spherical
    longitude = np.arctan2(Y, X)
    latitude = np.arctan2(Z, np.sqrt(X**2 + Y**2))
    radius = np.sqrt(X ** 2 + Y ** 2 + Z ** 2)
    return np.rad2deg(longitude), np.rad2deg(latitude), radius


def transform_data(filename='data.csv'):
    """
    Andrew please write a summary here
    
    Returns
    -------
    [type]
        [description]
    """

    df = pd.read_csv(filename, usecols = INPUTCOLS)
    df.columns = COLS
    df['epoch'] = pd.to_datetime(df['epoch'])
    # convert rev/day to degree/sec
    df['mean_motion'] = df['mean_motion'] * 360 / 86400 
    df['mean_anom'] = df[['mean_motion', 'epoch', 'mean_anom_epoch']].apply(get_mean_anomaly, axis=1)
    sphere_coords = np.empty((len(df), 3))
    for i, row in df[['a', 'ecc', 'inc', 'raan', 'arg_perigee', 'mean_anom']].iterrows():
        sphere_coords[i] = convert_coords(*row.values)
    df['longitude'] = sphere_coords[:, 0]
    df['latitude'] = sphere_coords[:, 1]
    df['radius'] = sphere_coords[:, 2]
    df['catalog'] = df[['name', 'type', 'period', 'ecc', 'inc']].apply(get_category, axis=1)

    return df[['name', 'type', 'longitude', 'latitude', 'radius']]


if __name__ == '__main__':
    path = os.path.dirname(__file__)
    filename = os.path.join(path, 'data.csv')
    transformed_data = transform_data(filename)
    transformed_data.to_pickle(app.config['data_file'], protocol=2)
