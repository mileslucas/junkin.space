import os

from flask import render_template, request, jsonify
import pandas as pd
import numpy as np

from . import app


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/api/data')
def api_data():
    df = pd.read_pickle(app.config['data_file'])[::2]
    
    payload = {
        'payload': df[df['type'] == 'PAYLOAD'].to_dict('list'),
        'rocket body': df[df['type'] == 'ROCKET BODY'].to_dict('list'),
        'debris': df[df['type'] == 'DEBRIS'].to_dict('list'),
        'tba': df[df['type'] == 'TBA'].to_dict('list'),
        'all': df.to_dict('list')
    }

    return jsonify(payload)


@app.route('/api/prob')
def api_prob():
    lat = float(request.args['lat'])
    lon = float(request.args['lon'])
    accuracy = float(request.args['accuracy']) / 111111  # meters to degrees-ish
    epsilon = 8 # degree radius

    df = pd.read_pickle(app.config['data_file'])
    min_lat = lat - epsilon
    max_lat = lat + epsilon
    min_lon = lon - epsilon
    max_lon = lon + epsilon
    total = len(df)
    mask = (df['latitude'] > min_lat) & (df['latitude'] < max_lat) \
        & (df['longitude'] > min_lon) & (df['longitude'] < max_lon)
    prob = sum(mask) / total
    if prob == 0:
        prob = np.finfo(np.double).eps
    odds = int(round(1/prob))

    return jsonify(f'{odds:,d}')
