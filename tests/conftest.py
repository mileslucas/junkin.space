import pytest

from spacejunk import app as myapp

@pytest.fixture
def app():
    yield myapp