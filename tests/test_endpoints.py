import pytest

@pytest.mark.parametrize('endpoint', [
    '/',
    '/about',
    '/api/data',
    # '/api/prob',
])
def test_endpoints(client, endpoint):
    rv = client.get(endpoint)
    assert rv.status_code == 200
