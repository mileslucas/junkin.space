# Space Junk

* Andrew Chatman
* Mitchell Kazin
* Miles Lucas

## Contributing

If you want to contribute to this project, use `pipenv` to set up a local development environment.

    $ pip install pipenv 
    $ pipenv install -d

This will create a virtual environment with all of the python packages for development. To enter this environment use 
    
    $ pipenv shell

You should now be in the virtual environment and ready to develop.

If you want to add a package to the virtual environment, use

    $ pipenv install <package-name>

Or if it is something meant for development (not production) use

    $ pipenv install -d <package-name>

To run a version of the development server

    $ python run.py